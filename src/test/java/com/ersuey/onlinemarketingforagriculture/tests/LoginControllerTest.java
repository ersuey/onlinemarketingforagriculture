package com.ersuey.onlinemarketingforagriculture.tests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ersuey.onlinemarketingforagriculture.controllers.LoginController;

@RunWith(SpringRunner.class)
@WebMvcTest(LoginController.class)
public class LoginControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testLoginControllerPage() throws Exception {
		mockMvc.perform(get("/login"))
				.andExpect(view().name("login"));
	}
	
	@Test
	public void testAccessDenied() throws Exception {
		mockMvc.perform(get("/access-denied"))
				.andExpect(view().name("access_denied"));
	}
}