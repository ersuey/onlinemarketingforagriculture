package com.ersuey.onlinemarketingforagriculture.tests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ersuey.onlinemarketingforagriculture.controllers.AddFarmerController;

@RunWith(SpringRunner.class)
@WebMvcTest(AddFarmerController.class)
public class AddFarmerControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testAddFarmerControllerPage() throws Exception {
		mockMvc.perform(get("/addFarmer"))
				.andExpect(view().name("addfarmer"));
	}
	
	@Test
	public void testonAddFarmer() throws Exception {
		mockMvc.perform(post("/addFarmer"))
				.andExpect(view().name("viewFarmers"));
	}
	
}