package com.ersuey.onlinemarketingforagriculture.tests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ersuey.onlinemarketingforagriculture.controllers.FarmerPagesController;

@RunWith(SpringRunner.class)
@WebMvcTest(FarmerPagesController.class)
public class FarmerPagesControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testLoginControllerPage() throws Exception {
		mockMvc.perform(get("/myCrops"))
				.andExpect(view().name("mycrops"));
	}
	
	@Test
	public void testOnDiscardCrop() throws Exception {
		mockMvc.perform(get("/removeCrop"))
				.andExpect(view().name("mycrops"));
	}
	
	@Test
	public void testOnUpdateCrop() throws Exception {
		mockMvc.perform(get("/updateCrop"))
				.andExpect(view().name("updatecropinfo"));
	}
	
	@Test
	public void testLoginControllerPagePost() throws Exception {
		mockMvc.perform(post("/updateCropInfo"))
				.andExpect(view().name("mycrops"));
	}
	
}