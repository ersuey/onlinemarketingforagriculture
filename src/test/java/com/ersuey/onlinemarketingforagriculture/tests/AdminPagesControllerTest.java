package com.ersuey.onlinemarketingforagriculture.tests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ersuey.onlinemarketingforagriculture.controllers.AdminPagesController;

@RunWith(SpringRunner.class)
@WebMvcTest(AdminPagesController.class)
public class AdminPagesControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testLoginControllerPage() throws Exception {
		mockMvc.perform(get("/viewFarmers"))
				.andExpect(view().name("viewfarmers"));
	}
	
	@Test
	public void testviewFarmers() throws Exception {
		mockMvc.perform(get("/viewFarmers"))
				.andExpect(view().name("viewfarmers"));
	}
	
	@Test
	public void testRemoveFarmer() throws Exception {
		mockMvc.perform(get("/removeFarmer"))
				.andExpect(view().name("viewfarmers"));
	}
	
	@Test
	public void testviewAllCrops() throws Exception {
		mockMvc.perform(get("/viewAllCrops"))
				.andExpect(view().name("viewallcrops"));
	}
	
	@Test
	public void testOnadminRegister() throws Exception {
		mockMvc.perform(get("/register"))
				.andExpect(view().name("adminReg"));
	}
	

	@Test
	public void testOnadminRegisterPost() throws Exception {
		mockMvc.perform(post("/register"))
				.andExpect(view().name("viewfarmers"));
	}
	
	@Test
	public void testonAdminRemovedCrop() throws Exception {
		mockMvc.perform(get("/adminRemoveCrop"))
				.andExpect(view().name("viewallcrops"));
	}
	
	@Test
	public void testOnUpdateFarmer() throws Exception {
		mockMvc.perform(get("/updateFarmer"))
				.andExpect(view().name("updatefarmerinfo"));
	}
	
	@Test
	public void testOnUpdateFarmerInfo() throws Exception {
		mockMvc.perform(post("/updateFarmerInfo"))
				.andExpect(view().name("viewfarmers"));
	}
}