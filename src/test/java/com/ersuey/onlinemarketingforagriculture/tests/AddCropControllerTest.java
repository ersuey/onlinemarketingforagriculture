package com.ersuey.onlinemarketingforagriculture.tests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ersuey.onlinemarketingforagriculture.controllers.AddCropController;

@RunWith(SpringRunner.class)
@WebMvcTest(AddCropController.class)
public class AddCropControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testAddCropControllerPage() throws Exception {
		mockMvc.perform(get("/addCrop"))
				.andExpect(view().name("addCrop"));
	}
	
	@Test
	public void testonAddCrop() throws Exception {
		mockMvc.perform(post("/addCrop"))
				.andExpect(view().name("addCrop"));
	}
	
	@Test
	public void testonVerifyCrop() throws Exception {
		mockMvc.perform(get("/verify"))
				.andExpect(view().name("verifycrops"));
	}
	
	@Test
	public void testonDiscardCrop() throws Exception {
		mockMvc.perform(get("/discard"))
				.andExpect(view().name("verifycrops"));
	}
}