package com.ersuey.onlinemarketingforagriculture.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.ersuey.onlinemarketingforagriculture.security.SuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	  @Override
	  protected void configure(AuthenticationManagerBuilder auth)
	      throws Exception {
		  auth.userDetailsService(userDetailsService)
	          .passwordEncoder(bCryptPasswordEncoder);
	  } 
	  
	  @Bean
      public AuthenticationSuccessHandler myAuthenticationSuccessHandler(){
	      return new SuccessHandler();
	  }
	  
	  @Override
	  protected void configure(HttpSecurity http) throws Exception {
		 
		 http.authorizeRequests()
		     .antMatchers("/").permitAll()
		     .antMatchers("/login").permitAll()
		     .antMatchers("/viewFarmers").permitAll()
		     .antMatchers("/addFarmer").permitAll()
		     .antMatchers("/register").permitAll()
		     .antMatchers("/viewAllCrops").permitAll()
		     .antMatchers("/verifyCrops").permitAll()
		     .antMatchers("addCrop/verify", "/search").permitAll()
		     .antMatchers("/addCrop/discard", "/actuator/**").permitAll()
		     .antMatchers("/adminRemoveCrop", "/updateFarmer","/updateFarmerInfo").permitAll()
		     .antMatchers("/myCrops").hasAuthority("FARMER")
		     .antMatchers("/addCrop").hasAuthority("FARMER")
		     .antMatchers("/updateCropInfo", "/updateCrop").hasAuthority("FARMER")
		     .antMatchers("/removeCrop").hasAuthority("FARMER")
		     .anyRequest().authenticated()
		     .and()
		     	.formLogin()
		  
		     	.loginPage("/login")
		     			.defaultSuccessUrl("/myCrops")
		     			.failureUrl("/login?error=true")
		     			.successHandler(myAuthenticationSuccessHandler())
		     .and()
		     	.logout()
		     			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
		     			.logoutSuccessUrl("/")
	
	     	.and()
		     	.exceptionHandling()
		     	.accessDeniedPage("/accessdenied");
	   
	  }
	 
	 @Override
	 public void configure(WebSecurity webSecurity) throws Exception {
		 
		 webSecurity.ignoring()
		 			.antMatchers("/resources/**","/static/**","/css/**","/js/**","/images/**");
		 
	 }
}
