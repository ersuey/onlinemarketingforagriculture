package com.ersuey.onlinemarketingforagriculture.security;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Admin {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long adminId;	
	
	private String username;
	private String password;
	
	public Admin(String username,String password) {
		this.username = username;
		this.password = password;
	}
}
