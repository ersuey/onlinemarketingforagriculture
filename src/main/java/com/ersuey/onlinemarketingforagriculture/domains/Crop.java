package com.ersuey.onlinemarketingforagriculture.domains;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import lombok.Data;

@Data
@Entity
public class Crop {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Long cropId;
	
	@Size(min=2,message="Crop name is too short!")
	private String cropName;
	
	@NotNull(message="price Can not be null")
	@Positive(message="must be positve")
	private Double price;
	
	@NotNull(message="description Can not be null")
	@Size(min=4, message="Description is too short")
	private String description;
	
	@NotNull(message="place Can not be null")
	@Size(min=2, message="Place name is too short")
	private String place;	
	
	@OneToOne(targetEntity=Farmer.class)
	private Farmer farmer;
	
	private boolean isNotVerified = true;
}
