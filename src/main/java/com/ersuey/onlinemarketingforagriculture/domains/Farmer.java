package com.ersuey.onlinemarketingforagriculture.domains;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import lombok.Data;

@Data
@Entity
public class Farmer {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Long farmerId;
	
	@NotNull(message="first name Can not be null")
	@Size(min=2,message="first name is too short!")
	private String firstName;
	
	@NotNull(message="last name Can not be null")
	@Size(min=2,message="last name is too short!")
	private String lastName;
	
	@NotNull(message="region Can not be null")
	@Size(min=2, message="Region Name is too short")
	private String region;
	
	@NotNull(message="zone Can not be null")
	@Size(min=2, message="Zone name is too short")
	private String zone;
	
	@NotNull(message="kebele Can not be null")
	@Size(min=1, max=3, message="Invalid Kebele Number")
	private String kebele;
	
	@NotNull(message="house number Can not be null")
	@Size(min=1, max=4, message="House number is not Valid")
	private String houseNumber;
	
	@NotNull(message="phone number Can not be null")
	@Positive(message="Enter a valid phone number")
	private Long phoneNumber;
	
	@OneToMany(targetEntity=Crop.class)
	private List<Crop> crops;
	
	@NotNull(message="username Can not be null")
	private String username;
	
	@NotNull(message="password Can not be null")
	private String password;
	
	
	
}
