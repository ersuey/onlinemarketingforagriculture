package com.ersuey.onlinemarketingforagriculture.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ersuey.onlinemarketingforagriculture.domains.Farmer;
import com.ersuey.onlinemarketingforagriculture.security.Admin;
import com.ersuey.onlinemarketingforagriculture.services.CropService;
import com.ersuey.onlinemarketingforagriculture.services.FarmerService;
import com.ersuey.onlinemarketingforagriculture.services.adminService;

@Controller
public class AdminPagesController {
	
	@Autowired
	CropService cropService;
	
	@Autowired
	FarmerService farmerService;
	
	@Autowired
	adminService adminService;

	@GetMapping("/viewFarmers")
	public String goToviewFarmers(Model model) {
		model.addAttribute("farmers", farmerService.findAll());
		return "viewfarmers";
	}
	
	@GetMapping("/removeFarmer")
	public String onRemoveFarmer(@PathVariable @RequestParam(name = "id",defaultValue="1")Long id) {
		farmerService.deleteFarmer(id);
		return "redirect:/viewFarmers";		
	}
	
	@GetMapping("/viewAllCrops")
	public String goToViewAllCrops(Model model) {
		model.addAttribute("crops", cropService.findCropByIsNotVerified(false));
		return "viewallcrops";
	}
	
	@GetMapping("/verifyCrops")
	public String goToVerifyCrops(Model model) {
		model.addAttribute("crops", cropService.findCropByIsNotVerified(true));
		return "verifycrops";
	}
	
	@GetMapping("/register")
	public String OnadminRegister(Model model) {
		model.addAttribute("admin", new Admin());
		return "adminReg";
	}
	
	@PostMapping("/register")
	public String onAddFarmer(@Valid() @ModelAttribute("admin") Admin admin, Errors error, Model model){
		if(error.hasErrors()) {
			return "adminReg";
		}
		
		adminService.saveAdmin(admin);
		return "redirect:/viewFarmers";
	}
	
	@GetMapping("/adminRemoveCrop")
	public String onAdminRemovedCrop(@PathVariable @RequestParam(name = "id",defaultValue="1")Long id) {
		cropService.deleteCrop(id);
		return "redirect:/viewAllCrops";		
	}
	
	@GetMapping("/updateFarmer")
	public String onUpdateFarmer(@RequestParam("id") Long id, Model model) {
		Optional<Farmer> farmerOpt = farmerService.findFarmerById(id);
		if(farmerOpt.isPresent()) {
			model.addAttribute("updateFarmer", farmerOpt.get());
		}
		return "/updateFarmerInfo";
	}
	
	@PostMapping("/updateFarmerInfo")
	public String onUpdateFarmerInfo(@Valid @ModelAttribute("updateFarmer") Farmer farmer, BindingResult  bindingResult) {
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult.getAllErrors().get(0).getDefaultMessage());
			return "updatefarmerinfo";
		}
		Farmer f = farmerService.findFarmerById(farmer.getFarmerId()).get();
		farmer.setUsername(f.getUsername());
		farmer.setPassword(f.getPassword());
		farmerService.updateFarmer(farmer);
		return "redirect:/viewFarmers";
	}
}

















