package com.ersuey.onlinemarketingforagriculture.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ersuey.onlinemarketingforagriculture.services.CropService;

@Controller
@RequestMapping("/")
public class HomeController {
	
	@Autowired
	CropService cropService;
	
	@GetMapping
	public String home(Model model) {
		model.addAttribute("crops", cropService.findCropByIsNotVerified(false));
		return "index";
	}
	
	@GetMapping("/accessdenied")
	public String accessDenied() {
		return "accessdenied";
	}
	
	@PostMapping("/search")
	public String findCrop(@RequestParam("keyWord") String key, Model model) {
		model.addAttribute("crops", cropService.findByCropNameContains(key));
		return "index";
	}
}






