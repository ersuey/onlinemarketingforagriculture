package com.ersuey.onlinemarketingforagriculture.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {
	
	@GetMapping("/login")
	public String onAdminLogin() {
		return "login";
	}
	
	@GetMapping("/access-denied")
    public String accessDenied(){
        return "access_denied";
    }
	
}
