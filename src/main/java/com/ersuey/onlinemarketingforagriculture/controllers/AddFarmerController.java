package com.ersuey.onlinemarketingforagriculture.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ersuey.onlinemarketingforagriculture.domains.Farmer;
import com.ersuey.onlinemarketingforagriculture.services.FarmerService;

@Controller
@RequestMapping("/addFarmer")
public class AddFarmerController {

	@Autowired
	FarmerService farmerService;
	
	@GetMapping
	public String onAddFarmerForm(Model model) {
		model.addAttribute("farmer", new Farmer());
		return "addfarmer";
	}
	
	@PostMapping
	public String onAddFarmer(@Valid() @ModelAttribute("farmer") Farmer farmer, Errors error, Model model){
		if(error.hasErrors()) {
			return "addfarmer";
		}
		
		farmerService.saveFarmer(farmer);
		return "redirect:/viewFarmers";
	}
	
}
