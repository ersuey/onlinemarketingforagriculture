package com.ersuey.onlinemarketingforagriculture.controllers;

import java.security.Principal;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ersuey.onlinemarketingforagriculture.domains.Farmer;
import com.ersuey.onlinemarketingforagriculture.services.CropService;
import com.ersuey.onlinemarketingforagriculture.services.FarmerService;
import com.ersuey.onlinemarketingforagriculture.domains.Crop;

@Controller
public class FarmerPagesController {
	
	@Autowired
	CropService cropService;
	
	@Autowired
	FarmerService farmerService;
	
	@GetMapping("/myCrops")
	public String goToMyCrops(Model model,Principal pricipal) {
		Farmer farmer = farmerService.findFarmerByUsername(pricipal.getName());
		model.addAttribute("crops", cropService.findByFarmerAndByVerfication(farmer));
		return "mycrops";
	}
	
	@GetMapping("/removeCrop")
	public String onDiscardCrop(@PathVariable @RequestParam(name = "id",defaultValue="1")Long id) {
		cropService.deleteCrop(id);
		return "redirect:/myCrops";		
	}
	
	@GetMapping("/updateCrop")
	public String onUpdateCrop(@RequestParam("id") Long id, Model model) {
		Optional<Crop> cropOpt = cropService.findCropById(id);
		if(cropOpt.isPresent()) {
			model.addAttribute("updateCrop", cropOpt.get());
		}
		return "/updateCropInfo";
	}
	
	@PostMapping("/updateCropInfo")
	public String onUpdateCropInfo(@Valid @ModelAttribute("updateCrop") Crop crop, BindingResult  bindingResult) {
		if (bindingResult.hasErrors()) {
			return "updatecropinfo";
		}
		cropService.updateCrop(crop);
		
		return "redirect:/myCrops";
	}
}
