package com.ersuey.onlinemarketingforagriculture.controllers;

import java.security.Principal;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ersuey.onlinemarketingforagriculture.domains.Crop;
import com.ersuey.onlinemarketingforagriculture.services.CropService;
import com.ersuey.onlinemarketingforagriculture.services.FarmerService;

@Controller
@RequestMapping("/addCrop")
public class AddCropController {
	
	@Autowired
	CropService cropService;
	
	@Autowired
	FarmerService farmerservice;
	
	@GetMapping
	public String onAddCropForm(Model model) {
		model.addAttribute("crop", new Crop());
		return "addcrop";
	}

	@PostMapping
	public String onAddCrop(@Valid Crop crop, Errors error, Model model,Principal principal){
		if(error.hasErrors()) {
			return "addcrop";
		}
		crop.setFarmer(farmerservice.findFarmerByUsername(principal.getName()));
		cropService.saveCrop(crop);
		return "redirect:/myCrops";
	}
	
	@GetMapping("/verify")
	public String onVerifyCrop(@PathVariable @RequestParam(name = "id",defaultValue="1")Long id) {
		Optional<Crop> crop = cropService.findCropById(id);
		crop.get().setNotVerified(false);
		cropService.saveCrop(crop.get());
		return "redirect:/verifyCrops";		
	}
	
	@GetMapping("/discard")
	public String onDiscardCrop(@PathVariable @RequestParam(name = "id",defaultValue="1")Long id) {
		cropService.deleteCrop(id);
		return "redirect:/verifyCrops";		
	}
	
}
