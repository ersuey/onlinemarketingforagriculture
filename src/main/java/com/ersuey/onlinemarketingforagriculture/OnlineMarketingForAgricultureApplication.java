package com.ersuey.onlinemarketingforagriculture;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.ersuey.onlinemarketingforagriculture.security.Role;
import com.ersuey.onlinemarketingforagriculture.services.RoleService;
import com.ersuey.onlinemarketingforagriculture.services.adminService;

@SpringBootApplication
public class OnlineMarketingForAgricultureApplication extends SpringBootServletInitializer  {
	
	

	public static void main(String[] args) {
		SpringApplication.run(OnlineMarketingForAgricultureApplication.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(OnlineMarketingForAgricultureApplication.class); }
	@Bean
	public CommandLineRunner dataLoader(RoleService roleService, adminService adminService) {
	    return new CommandLineRunner() {
	      @Override
	      public void run(String... args) throws Exception {	   	    	  	    	   
	    	  roleService.save(new Role(1L, "FARMER"));
	    	  roleService.save(new Role(2L, "ADMIN"));	    	  	    		    	  
	      }
	    };
	}
}
