package com.ersuey.onlinemarketingforagriculture.services;

import java.util.Optional;

import com.ersuey.onlinemarketingforagriculture.domains.Crop;
import com.ersuey.onlinemarketingforagriculture.domains.Farmer;

public interface CropService {
	Iterable<Crop> findByFarmerAndByVerfication(Farmer farmer); 
	void saveCrop(Crop crop);
	Optional<Crop> findCropById(Long id);
	Iterable<Crop> findAll();
	void deleteCrop(Long id);
	Iterable<Crop> findCropByIsNotVerified(boolean isNotVerfied);
	public void updateCrop(Crop crop);
	Iterable<Crop> findByCropNameContains(String cropName);
}
