package com.ersuey.onlinemarketingforagriculture.services;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.ersuey.onlinemarketingforagriculture.security.User;

public interface UserService extends UserDetailsService {
		User findUserByUsername(String username);
		void saveUser(User user,String role);
}
