package com.ersuey.onlinemarketingforagriculture.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ersuey.onlinemarketingforagriculture.domains.Crop;
import com.ersuey.onlinemarketingforagriculture.domains.Farmer;
import com.ersuey.onlinemarketingforagriculture.repositories.CropRepository;

@Service
public class CropServiceImpl implements CropService {

	@Autowired
	private CropRepository cropRepo;
	
	@Autowired
	CropServiceImpl(CropRepository cropRepo){
		this.cropRepo = cropRepo;
	}
	
	@Override
	public void saveCrop(Crop crop) {
		cropRepo.save(crop);		
	}

	@Override
	public Optional<Crop> findCropById(Long id) {
		return cropRepo.findById(id);
	}

	@Override
	public Iterable<Crop> findAll() {
		return cropRepo.findAll();
	}

	@Override
	public void deleteCrop(Long id) {
		cropRepo.deleteById(id);
		
	}

	@Override
	public Iterable<Crop> findCropByIsNotVerified(boolean isNotVerfied) {
		return cropRepo.findByIsNotVerified(isNotVerfied);
	}

	@Override
	public Iterable<Crop> findByFarmerAndByVerfication(Farmer farmer) {
		return cropRepo.findByFarmerAndIsNotVerified(farmer, false);
	}

	@Override
	public void updateCrop(Crop crop) {
		cropRepo.save(crop);
		
	}

	@Override
	public Iterable<Crop> findByCropNameContains(String cropName) {
		Iterable<Crop> crop = cropRepo.findByCropNameContains(cropName);
		return crop;
	}	
}
