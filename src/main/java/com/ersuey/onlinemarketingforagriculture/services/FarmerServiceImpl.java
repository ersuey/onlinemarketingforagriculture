package com.ersuey.onlinemarketingforagriculture.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ersuey.onlinemarketingforagriculture.domains.Farmer;
import com.ersuey.onlinemarketingforagriculture.repositories.FarmerRepository;
import com.ersuey.onlinemarketingforagriculture.security.User;

@Service
public class FarmerServiceImpl implements FarmerService{

	@Autowired
	private FarmerRepository farmerRepo;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	FarmerServiceImpl(FarmerRepository farmerRepo){
		this.farmerRepo = farmerRepo;
	}
	
	@Override
	public void saveFarmer(Farmer farmer) {
		farmer.setPassword(bCryptPasswordEncoder.encode(farmer.getPassword()));
		farmerRepo.save(farmer);
		User user = new User();
		user.setUsername(farmer.getUsername());
		user.setPassword(farmer.getPassword());
		userService.saveUser(user, "FARMER");
	}
	

	public void updateFarmer(Farmer farmer) {
		farmerRepo.save(farmer);
	}

	@Override
	public Iterable<Farmer> findAll() {
		return farmerRepo.findAll();
	}

	@Override
	public Optional<Farmer> findFarmerById(Long id) {
		return farmerRepo.findById(id);
	}

	@Override
	public Farmer findFarmerByUsername(String username) {
		return farmerRepo.findByUsername(username);
	}

	@Override
	public void deleteFarmer(Long id) {
		farmerRepo.deleteById(id);
	}

}
