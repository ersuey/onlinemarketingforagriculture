package com.ersuey.onlinemarketingforagriculture.services;

import java.util.Optional;

import com.ersuey.onlinemarketingforagriculture.domains.Farmer;

public interface FarmerService {
	Farmer findFarmerByUsername(String Username);
	void saveFarmer(Farmer farmer);
	Optional<Farmer> findFarmerById(Long id);
	Iterable<Farmer> findAll();
	void deleteFarmer(Long id);
	public void updateFarmer(Farmer farmer);
}
