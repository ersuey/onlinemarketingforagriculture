package com.ersuey.onlinemarketingforagriculture.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ersuey.onlinemarketingforagriculture.repositories.AdminRepository;
import com.ersuey.onlinemarketingforagriculture.security.Admin;
import com.ersuey.onlinemarketingforagriculture.security.User;

@Service
public class AdminServiceImpl implements adminService{
	
	@Autowired
	AdminRepository adminRepo;
	
	@Autowired
	UserService userService;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public void saveAdmin(Admin admin) {
		admin.setPassword(bCryptPasswordEncoder.encode(admin.getPassword()));
		adminRepo.save(admin);
		User user = new User();
		user.setUsername(admin.getUsername());
		user.setPassword(admin.getPassword());	
		userService.saveUser(user, "ADMIN");
	}

}
