package com.ersuey.onlinemarketingforagriculture.repositories;

import org.springframework.data.repository.CrudRepository;

import com.ersuey.onlinemarketingforagriculture.security.Admin;

public interface AdminRepository extends CrudRepository<Admin, Long> {

}
