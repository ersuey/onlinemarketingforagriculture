package com.ersuey.onlinemarketingforagriculture.repositories;
import org.springframework.data.repository.CrudRepository;

import com.ersuey.onlinemarketingforagriculture.security.User;

public interface UserRepository extends CrudRepository<User, Long> {
  User findByUsername(String username);
}
