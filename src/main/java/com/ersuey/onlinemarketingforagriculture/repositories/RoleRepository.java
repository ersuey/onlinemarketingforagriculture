package com.ersuey.onlinemarketingforagriculture.repositories;

import org.springframework.data.repository.CrudRepository;

import com.ersuey.onlinemarketingforagriculture.security.Role;

public interface RoleRepository extends CrudRepository<Role, Long>{
	Role findByRole(String role);
}
