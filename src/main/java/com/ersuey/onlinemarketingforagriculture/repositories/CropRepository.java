package com.ersuey.onlinemarketingforagriculture.repositories;

import org.springframework.data.repository.CrudRepository;
import com.ersuey.onlinemarketingforagriculture.domains.Crop;
import com.ersuey.onlinemarketingforagriculture.domains.Farmer;

public interface CropRepository extends CrudRepository<Crop, Long> {
	Iterable<Crop> findByFarmerAndIsNotVerified(Farmer farmer,boolean isNotVerified);
	Iterable<Crop> findByIsNotVerified(boolean isNotVerfied);
	Iterable<Crop> findByCropNameContains(String cropName);
}
