package com.ersuey.onlinemarketingforagriculture.repositories;

import org.springframework.data.repository.CrudRepository;
import com.ersuey.onlinemarketingforagriculture.domains.Farmer;

public interface FarmerRepository extends CrudRepository<Farmer, Long> {
	Farmer findByUsername(String username);
}
